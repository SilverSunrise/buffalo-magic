package com.buffalomag.ic

import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*


class MainActivity : AppCompatActivity() {

    lateinit var timerBuffalo: CountDownTimer
    var marginPlayer = 5
    var marginBuffalo = 5
    var marginFinish = 5
    var difficult: Long = 250


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val (playerLayoutParams, buffaloLayoutParams, step) = getDataForWin()

        getCharacterChangeData()
        moveSettingsPlayer(playerLayoutParams, step)
        moveSettingsBuffalo(buffaloLayoutParams, step)
        menuOptions(playerLayoutParams)
    }

    private fun moveSettingsPlayer(
        playerLayoutParams: ConstraintLayout.LayoutParams,
        step: Int
    ) {
        btn_click.setOnClickListener {
            playerLayoutParams.marginStart = marginPlayer
            clickAnimation()
            if (marginPlayer <= marginFinish) {
                marginPlayer += step
                iv_player.layoutParams = playerLayoutParams
            } else {
                cl_settingsMenu.visibility = View.VISIBLE
                tv_gameState.text = "You Win"
                timerBuffalo.cancel()
                btn_click.isEnabled = false
                btn_startGame.visibility = View.VISIBLE
                btn_pause.visibility = View.GONE
            }
        }
    }
    @Suppress("DEPRECATION")
    private fun getDataForWin(): Triple<ConstraintLayout.LayoutParams, ConstraintLayout.LayoutParams, Int> {
        val playerLayoutParams = iv_player.layoutParams as ConstraintLayout.LayoutParams
        val buffaloLayoutParams = iv_buffalo.layoutParams as ConstraintLayout.LayoutParams
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width: Int = size.x
        val step = width / 100

        marginFinish = width - (step * 10)
        return Triple(playerLayoutParams, buffaloLayoutParams, step)
    }

    private fun menuOptions(playerLayoutParams: ConstraintLayout.LayoutParams) {
        tv_gameState.text = "Menu"
        btn_startGame.visibility = View.VISIBLE
        cl_settingsMenu.visibility = View.VISIBLE
        btn_restart.visibility = View.GONE

        btn_startGame.setOnClickListener {
            playerLayoutParams.marginStart = 5
            iv_player.layoutParams = playerLayoutParams
            marginPlayer = 5
            timerBuffalo.onFinish()
            countdown()
            btn_startGame.visibility = View.GONE
            cl_settingsMenu.visibility = View.GONE
        }

        btn_restart.setOnClickListener {
            playerLayoutParams.marginStart = 5
            iv_player.layoutParams = playerLayoutParams
            marginPlayer = 5
            timerBuffalo.onFinish()
            cl_settingsMenu.visibility = View.GONE
            countdown()
        }

        btn_pause.setOnClickListener {

            tv_gameState.text = "Pause"
            timerBuffalo.cancel()
            cl_settingsMenu.visibility = View.VISIBLE
            btn_resume.visibility = View.VISIBLE
            btn_restart.visibility = View.VISIBLE
            btn_startGame.visibility = View.GONE

            btn_click.isEnabled = false

        }

        btn_resume.setOnClickListener {
            cl_settingsMenu.visibility = View.GONE
            btn_resume.visibility = View.GONE
            countdown()
        }
    }

    private fun moveSettingsBuffalo(
        buffaloLayoutParams: ConstraintLayout.LayoutParams,
        step: Int
    ) {
        timerBuffalo = object : CountDownTimer(Long.MAX_VALUE, difficult) {

            override fun onTick(millisUntilFinished: Long) {
                buffaloLayoutParams.marginStart = marginBuffalo
                if (marginBuffalo <= marginFinish) {
                    marginBuffalo += step
                    Log.d("TAG", marginBuffalo.toString())
                    iv_buffalo.layoutParams = buffaloLayoutParams
                } else {
                    cl_settingsMenu.visibility = View.VISIBLE
                    btn_click.isEnabled = false
                    tv_gameState.text = "You lose"
                    btn_startGame.visibility = View.VISIBLE
                    btn_pause.visibility = View.GONE
                }
            }

            override fun onFinish() {
                buffaloLayoutParams.marginStart = 5
                marginBuffalo = 5
                iv_buffalo.layoutParams = buffaloLayoutParams
                btn_click.isEnabled = false
                timerBuffalo.cancel()
            }
        }
    }

    private fun getCharacterChangeData() {
        val bundle1 = intent.extras
        if (bundle1 != null) {
            val picture = bundle1.getInt("playerCharacter")
            iv_player.setImageResource(picture)
        }

        val bundle2 = intent.extras
        if (bundle2 != null) {
            val picture = bundle2.getInt("enemyCharacter")
            iv_buffalo.setImageResource(picture)
        }

        val bundle3 = intent.extras
        if (bundle3 != null) {
            val updDifficult = bundle3.getInt("enemyDifficult", 250)
            difficult = updDifficult.toLong()
        }
        btn_characters.setOnClickListener {
            val intent = Intent(this@MainActivity, CharactersActivity::class.java)
            startActivity(intent)
        }
    }

    private fun clickAnimation() {
        btn_click.animate().apply {
            duration = 30
            scaleX(0.97f)
            scaleY(0.97f)
        }.withEndAction {
            btn_click.animate().apply {
                duration = 30
                scaleX(1.03f)
                scaleY(1.03f)
            }.start()
        }
    }

    private fun countdown() {
        btn_pause.visibility = View.GONE
        btn_click.isEnabled = false
        tv_countdown.visibility = View.VISIBLE
        tv_countdown.animate().apply {
            duration = 500
            scaleX(0.70f)
            scaleY(0.70f)
            tv_countdown.text = 3.toString()
        }.withEndAction {
            tv_countdown.animate().apply {
                duration = 500
                scaleX(1.20f)
                scaleY(1.20f)
                tv_countdown.text = 2.toString()
            }.withEndAction {
                tv_countdown.animate().apply {
                    duration = 500
                    scaleX(0.70f)
                    scaleY(0.70f)
                    tv_countdown.text = 1.toString()
                }.withEndAction {
                    tv_countdown.animate().apply {
                        duration = 500
                        scaleX(1f)
                        scaleY(1f)
                        tv_countdown.text = "Let`s go"
                        timerBuffalo.start()
                        btn_click.isEnabled = true
                    }.withEndAction {
                        tv_countdown.animate().apply {
                            btn_pause.visibility = View.VISIBLE
                            duration = 100
                            tv_countdown.visibility = View.GONE
                        }.start()
                    }
                }
            }
        }
    }
}