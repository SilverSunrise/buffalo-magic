package com.buffalomag.ic

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_characters.*
import kotlinx.android.synthetic.main.activity_main.*


class CharactersActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_characters)
        val intent = Intent(this, MainActivity::class.java)

        rg_playerCharacters.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_playerCharacter1 -> intent.putExtra(
                    "playerCharacter",
                    R.drawable.player_lion
                )
                R.id.rb_playerCharacter2 -> intent.putExtra(
                    "playerCharacter",
                    R.drawable.player_pantera
                )
            }
        }

        rg_enemyCharacters.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_enemyCharacter1 -> {
                    intent.putExtra("enemyCharacter", R.drawable.buff_easy)
                    intent.putExtra("enemyDifficult", 235)
                }
                R.id.rb_enemyCharacter2 -> {
                    intent.putExtra("enemyCharacter", R.drawable.buff_normal)
                    intent.putExtra("enemyDifficult", 165)
                }
                R.id.rb_enemyCharacter3 -> {
                    intent.putExtra("enemyCharacter", R.drawable.buff_hard)
                    intent.putExtra("enemyDifficult", 100)
                }
            }
        }

        btn_completeCharacter.setOnClickListener {
            if (rg_enemyCharacters.checkedRadioButtonId == -1 || rg_playerCharacters.checkedRadioButtonId == -1) {
                showPointerHint()
            } else {
                startActivity(intent)
            }
        }

    }

    private fun showPointerHint() {
        tv_warn.animate().apply {
            duration = 60
            scaleX(0.90f)
            scaleY(0.90f)
        }.withEndAction {
            tv_warn.animate().apply {
                duration = 60
                scaleX(1.10f)
                scaleY(1.10f)
            }.withEndAction {
                tv_warn.animate().apply {
                    duration = 60
                    scaleX(1.00f)
                    scaleY(1.00f)
                }.start()
            }
        }
    }
}